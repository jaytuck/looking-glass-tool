import sys
import toml
import asyncio
import subprocess
from functools import partial
r = partial(subprocess.run, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


config = toml.load('config.toml')

# rate limit to 50 runs at a time
rate_limit_sem = asyncio.Semaphore(50)


async def telnet_connection_with_timeout(*args, timeout=60, **kwargs):
    try:
        await asyncio.wait_for(telnet_connection(*args, **kwargs), timeout=timeout)
    except asyncio.TimeoutError:
        print(f'telnet connection got timeout')


async def telnet_connection(reader, writer):
    try:
        async with rate_limit_sem:
            print(f'sem currently at: {rate_limit_sem}')
            async def w(t):
                t = t.replace('\n', '\r\n') # fix for cisco devices
                print(f'send:\n{t}')
                writer.write(f'{t}\r\n'.encode())
                await writer.drain()
            sock = writer.get_extra_info("socket")
            print(f'running for peer: {sock.getpeername()}')
            await w(f'I see connection on socket: {sock}')
            await w(f'performing traceroute')
            p = await asyncio.create_subprocess_exec('traceroute', '--max-hops=15', sock.getpeername()[0], stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.STDOUT)
            await w(f'result:')
            async def stream_output():
                while out := await p.stdout.read(1):
                    writer.write(out.replace(b'\n',b'\r\n'))
            streamer = asyncio.create_task(stream_output())
            await p.wait()  # wait for the proc to finish
            await streamer  # let the output finish streaming
            await writer.drain()
    finally:
        writer.close()
        sys.stdout.flush()


async def setup_telnet_server():
    serv = await asyncio.start_server(
        telnet_connection_with_timeout,
        host='0.0.0.0',
        port=config['telnet_port'],
    )
    async with serv:
        await serv.serve_forever()


def main():
    asyncio.run(setup_telnet_server())

if __name__ == "__main__":
    main()

